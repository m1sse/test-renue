FROM nginx:1.23.0-alpine

RUN ln -sf /dev/stdout /var/log/nginx/access.log \ 	&& ln -sf /dev/stderr /var/log/nginx/error.log  



COPY nginx.conf /etc/nginx/nginx.conf

CMD ["nginx-debug", "-g", "daemon off;"]

EXPOSE 8081

